import React from "react";
import validator from "validator";

class Checkout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSection: "address",
      address: {
        name: "Home",
        house: "",
        apartment: "",
        street: "",
        landmark: "",
        area: "",
        city: "",
        pincode: "",
      },
      addressError: {
        house: false,
        area: false,
        city: false,
        pincode: false,
      },
      creditCard: {
        number: "",
        valid: "",
        cvv: "",
      },
      showSuccess: false,
      creditCardError: {
        cvv: false,
        number: false,
        valid: false,
      },
    };
  }

  changeSection = (section) => {
    this.setState({
      currentSection: section,
    });
  };

  componentDidMount = () => {
    if (!this.state.showSuccess && (!this.props.isLogged || this.props.cart.size === 0)) {
      this.props.history.push("/cart");
    }
  };

  componentDidUpdate = () => {
    if (!this.state.showSuccess && (!this.props.isLogged || this.props.cart.size === 0)) {
      this.props.history.push("/cart");
    }
  };

  handleInputChange = (event) => {
    this.setState((state) => ({
      address: { ...state.address, [event.target.name]: event.target.value },
    }));
  };

  changeAddressName = (name) => {
    this.setState((state) => ({
      address: { ...state.address, name: name },
    }));
  };

  submitAddress = (event) => {
    event.preventDefault();
    this.setState(
      {
        addressError: {
          house: false,
          area: false,
          city: false,
          pincode: false,
        },
      },
      () => {
        let isError = false;
        if (this.state.address.house === "") {
          this.setState((state) => ({
            addressError: { ...state.addressError, house: true },
          }));
          isError = true;
        }

        if (this.state.address.area === "") {
          this.setState((state) => ({
            addressError: { ...state.addressError, area: true },
          }));
          isError = true;
        }

        if (this.state.address.city === "" || !validator.isAlpha(this.state.address.city)) {
          this.setState((state) => ({
            addressError: { ...state.addressError, city: true },
          }));
          isError = true;
        }

        if (this.state.address.pincode === "" || isNaN(Number(this.state.address.pincode)) || this.state.address.pincode.length !== 6) {
          this.setState((state) => ({
            addressError: { ...state.addressError, pincode: true },
          }));
          isError = true;
        }

        if (!isError) {
          this.changeSection("delivery");
        }
      }
    );
  };

  handleCardDetails = (event) => {
    if (event.target.value !== "") {
      if (event.target.name !== "valid") {
        console.log(event.target.value);
        if (!validator.isInt(event.target.value)) {
          return;
        }

        if (event.target.name === "number" && event.target.value.length > 16) {
          return;
        }

        if (event.target.name === "cvv" && event.target.value.length > 3) {
          return;
        }
      } else {
        if (event.target.value.length === 2) {
          event.target.value = event.target.value + "/";
        } else if (event.target.value.length === 3) {
          event.target.value = event.target.value.substring(0, 1);
        }
      }
    }

    this.setState((state) => ({
      creditCard: { ...state.creditCard, [event.target.name]: event.target.value },
    }));
  };

  submitCreditCard = () => {
    console.log("sdfsdf");
    let isError = false;
    this.setState(
      {
        creditCardError: {
          cvv: false,
          number: false,
          valid: false,
        },
      },
      () => {
        if (this.state.creditCard.number.length !== 16) {
          this.setState((state) => ({
            creditCardError: { ...state.creditCardError, number: true },
          }));
          isError = true;
        }

        if (
          this.state.creditCard.valid.length !== 5 ||
          !validator.isInt(this.state.creditCard.valid.split("/")[0]) ||
          !validator.isInt(this.state.creditCard.valid.split("/")[1])
        ) {
          this.setState((state) => ({
            creditCardError: { ...state.creditCardError, valid: true },
          }));
          isError = true;
        }

        if (this.state.creditCard.cvv.length !== 3) {
          this.setState((state) => ({
            creditCardError: { ...state.creditCardError, cvv: true },
          }));
          isError = true;
        }

        if (!isError) {
          this.setState({
            showSuccess: true,
          });
          this.props.clearCart();
        }
      }
    );
  };

  render() {
    let totalPrice = [...this.props.cart].reduce((acc, [key, item]) => {
      return acc + item.quantity * item.price.price;
    }, 0);
    if (!this.state.showSuccess) {
      return (
        <div className="container mt-md-5">
          <div className="row">
            <div className="col-md-9 col-12">
              {/* Mobile view order summary */}
              <div className="col-md-3 col-12 mt-3 mt-md-0 d-block d-md-none">
                <p className="fw-bold">Order Summary</p>
                <div className="order-border">
                  <div className="border px-2 py-2">
                    <div className="d-flex justify-content-between fs-15px">
                      <span className="d-block">Basket value</span>
                      <span className="">Rs {totalPrice}</span>
                    </div>
                    <div className="d-flex justify-content-between mt-2 fs-15px">
                      <span className="d-block fw-bold">Total Amount Payable</span>
                      <span className="fw-bold">Rs {totalPrice}</span>
                    </div>
                    <div className="d-flex justify-content-between fs-15px mt-2 border p-2 apple-green apple-green-dark-text fw-bold">
                      <span className="d-block">Total Savings</span>
                      <span>Rs {(totalPrice * ((Math.random() + Math.random() + 8) / 100)).toFixed(2)}</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="border mt-3 mt-md-0">
                <div className="d-flex align-items-center justify-content-between p-2 gap-1 text-secondary">
                  <div className="d-flex m-0 p-0 align-items-center gap-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="16" class="DeliveryAddress___StyledLocationIcon-sc-90wkue-2 dMhXuT">
                      <path
                        fill-rule="evenodd"
                        d="M6.397 14.893a.316.316 0 0 1-.436-.007C4.96 13.897.764 9.556.751 6.176.74 3.182 3.181.754 6.174.754 9.17.753 11.55 3.18 11.6 6.177c.06 3.593-4.183 7.764-5.203 8.716zm5.954-8.729a6.296 6.296 0 0 0-1.825-4.353A6.092 6.092 0 0 0 6.074.001a6.193 6.193 0 0 0-4.255 1.792A6.102 6.102 0 0 0 0 6.18c.006 1.76.97 3.899 2.874 6.355 1.375 1.776 2.73 3.053 2.787 3.107l.29.273c.117.11.301.114.42.007l.301-.264a25.55 25.55 0 0 0 2.83-3.023c1.923-2.42 2.88-4.598 2.85-6.471zM6.274 8.362zm0-5.627c-1.76 0-3.19 1.43-3.19 3.19 0 1.758 1.43 3.19 3.19 3.19s3.19-1.432 3.19-3.19c0-1.76-1.43-3.19-3.19-3.19z"
                        opacity="0.5"
                      ></path>
                    </svg>
                    <p className={"m-0" + (this.state.currentSection === "address" ? " fw-bold fs-5" : "")}>Delivery Address</p>
                  </div>

                  {this.state.currentSection !== "address" && (
                    <>
                      <div className="fs-15px text-secondary d-none d-lg-block">
                        <span>{this.state.address.name} - </span>
                        <span>{this.state.address.house}, </span>
                        <span>{this.state.address.area}, </span>
                        <span>{this.state.address.city}, </span>
                        <span>{this.state.address.pincode}</span>
                      </div>
                      <button
                        className="border px-md-5 border-dark py-1 fw-light fs-15px bg-white rounded float-end"
                        onClick={() => {
                          this.changeSection("address");
                        }}
                      >
                        Change
                      </button>
                    </>
                  )}
                </div>
                {this.state.currentSection === "address" && (
                  <div className="p-3 border-top">
                    <form className="d-flex flex-column justify-content-center align-items-between m-0 p-0">
                      <div className="row p-0 m-0 mt-3">
                        <div className="col-12 col-md-4 p-0">
                          <input
                            placeholder="House / Flat No"
                            className="border-0 border-bottom border-success p-0 w-100"
                            value={this.state.address.house}
                            name="house"
                            onChange={this.handleInputChange}
                          ></input>
                          {this.state.addressError.house && <p className="m-0 fs-12px text-danger">Enter valid house No</p>}
                        </div>
                        <div className="col-12 offset-md-1 col-md-4 p-0 mt-3 mt-md-0">
                          <input
                            placeholder="Apartment name"
                            className="border-0 border-bottom border-success p-0 w-100"
                            value={this.state.address.apartment}
                            name="apartment"
                            onChange={this.handleInputChange}
                          ></input>
                        </div>
                      </div>
                      <div className="row p-0 m-0 mt-3">
                        <div className="col-12 col-md-4 p-0">
                          <input
                            placeholder="Street details to locate you"
                            className="border-0 border-bottom border-success p-0 w-100"
                            value={this.state.address.street}
                            name="street"
                            onChange={this.handleInputChange}
                          ></input>
                        </div>
                        <div className="col-12 offset-md-1 col-md-4 p-0  mt-3 mt-md-0">
                          <input
                            placeholder="Landmark for easy reach out"
                            className="border-0 border-bottom border-success p-0 w-100"
                            value={this.state.address.landmark}
                            name="landmark"
                            onChange={this.handleInputChange}
                          ></input>
                        </div>
                      </div>
                      <div className="row p-0 m-0 mt-3">
                        <div className="col-12 col-md-4 p-0 ">
                          <input
                            placeholder="Area Details"
                            className="border-0 border-bottom border-success p-0 w-100"
                            value={this.state.address.area}
                            name="area"
                            onChange={this.handleInputChange}
                          ></input>
                          {this.state.addressError.area && <p className="fs-12px m-0 text-danger">Enter valid area</p>}
                        </div>
                        <div className="col-12 offset-md-1 col-md-4 p-0  mt-3 mt-md-0">
                          <input
                            placeholder="City"
                            className="border-0 border-bottom border-success p-0 w-100"
                            value={this.state.address.city}
                            name="city"
                            onChange={this.handleInputChange}
                          ></input>
                          {this.state.addressError.city && <p className="fs-12px m-0 text-danger">Enter valid city</p>}
                        </div>
                      </div>
                      <div className="col-12 col-md-4 p-0">
                        <input
                          placeholder="Pin Code"
                          className="border-0 border-bottom border-success p-0 mt-3 w-100"
                          value={this.state.address.pincode}
                          name="pincode"
                          onChange={this.handleInputChange}
                        ></input>
                        {this.state.addressError.pincode && <p className="fs-12px m-0 text-danger">Enter valid pincode</p>}
                      </div>
                      <p className="text-secondary mt-3">Choose a nick name</p>
                      <div className="row align-items-center gap-2 m-0">
                        <div
                          className={
                            "border col-3 col-md-2 col-lg-2 col-xl-1 p-2 text-center rounded " +
                            (this.state.address.name === "Home" ? "border-success apple-green-dark-text" : "")
                          }
                          onClick={() => this.changeAddressName("Home")}
                          role="button"
                        >
                          Home
                        </div>
                        <div
                          className={
                            "border col-3 col-md-2 col-lg-2 col-xl-1 p-2 text-center rounded " +
                            (this.state.address.name === "Office" ? "border-success apple-green-dark-text" : "")
                          }
                          onClick={() => this.changeAddressName("Office")}
                          role="button"
                        >
                          Office
                        </div>
                      </div>
                      <div className="w-100">
                        <button
                          className="col-4 col-md-3 col-lg-2 col-xl-2 p-1 border rounded apple-green apple-green-dark-text fw-bold float-end"
                          onClick={this.submitAddress}
                        >
                          Continue
                        </button>
                      </div>
                    </form>
                  </div>
                )}
              </div>
              <div className="border mt-3">
                <div className="d-flex align-items-center justify-content-between p-2 gap-1 text-secondary">
                  <div className="d-flex align-items-center gap-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" class="DeliveryOptions___StyledCalendarIcon-sc-z3tst0-2 hbKlct">
                      <path
                        fill-rule="evenodd"
                        d="M15.137 4.33H.847a.155.155 0 0 1-.155-.156v-.947c0-.086.07-.155.156-.155h14.289c.086 0 .155.07.155.155v.947c0 .086-.07.155-.155.155m0 11.479H.847a.155.155 0 0 1-.155-.156V5.16c0-.086.07-.155.156-.155h14.289c.086 0 .155.07.155.155v10.492c0 .086-.07.156-.155.156M12.55 2.224V.811A.31.31 0 0 0 12.24.5h-.124a.31.31 0 0 0-.311.31v1.414c0 .086-.07.156-.156.156H8.575a.155.155 0 0 1-.156-.156V.811A.31.31 0 0 0 8.11.5h-.125a.31.31 0 0 0-.31.31v1.414c0 .086-.07.156-.156.156H4.412a.155.155 0 0 1-.156-.156V.811A.31.31 0 0 0 3.946.5H3.82a.31.31 0 0 0-.31.31v1.414c0 .086-.07.156-.156.156H.311A.31.31 0 0 0 0 2.69v13.5c0 .17.14.31.31.31h15.364a.31.31 0 0 0 .31-.31V2.69a.31.31 0 0 0-.31-.31h-2.967a.155.155 0 0 1-.156-.156M8.94 13.735H6.92a.155.155 0 0 1-.155-.155v-2.02c0-.086.07-.155.155-.155h2.02c.086 0 .155.07.155.155v2.02c0 .085-.07.155-.155.155m-2.858-2.703v3.076c0 .171.139.31.31.31h3.076a.31.31 0 0 0 .31-.31v-3.076a.31.31 0 0 0-.31-.31H6.392a.31.31 0 0 0-.31.31m-1.593 2.687H2.47a.155.155 0 0 1-.154-.155v-2.02c0-.085.07-.155.155-.155h2.02c.085 0 .155.07.155.156v2.02c0 .085-.07.154-.156.154m-2.858-2.702v3.075c0 .172.14.31.31.31h3.076a.31.31 0 0 0 .311-.31v-3.075a.31.31 0 0 0-.31-.311H1.941a.31.31 0 0 0-.31.31M13.39 9.293h-2.02a.155.155 0 0 1-.154-.155v-2.02c0-.085.07-.155.155-.155h2.02c.085 0 .155.07.155.155v2.02c0 .086-.07.155-.156.155M10.532 6.59v3.076c0 .172.14.31.31.31h3.076a.31.31 0 0 0 .311-.31V6.589a.31.31 0 0 0-.31-.31h-3.076a.31.31 0 0 0-.311.31M8.94 9.292H6.92a.155.155 0 0 1-.155-.155v-2.02c0-.085.07-.155.155-.155h2.02c.086 0 .155.07.155.155v2.02c0 .086-.07.155-.155.155M6.082 6.59v3.076c0 .172.139.31.31.31h3.076a.31.31 0 0 0 .31-.31V6.589a.31.31 0 0 0-.31-.31H6.392a.31.31 0 0 0-.31.31M4.489 9.277H2.47a.155.155 0 0 1-.154-.156v-2.02c0-.085.07-.154.155-.154h2.02c.085 0 .155.07.155.155v2.02c0 .085-.07.155-.156.155M1.631 6.574V9.65c0 .171.14.31.31.31h3.076a.31.31 0 0 0 .311-.31V6.574a.31.31 0 0 0-.31-.31H1.941a.31.31 0 0 0-.31.31"
                      ></path>
                    </svg>
                    <p className={"m-0" + (this.state.currentSection === "delivery" ? " fw-bold fs-5" : "")}>Delivery Options</p>
                  </div>
                  {this.state.currentSection === "payment" && (
                    <>
                      <div className="fs-15px d-none d-lg-block">Delivery Option 1: Total Delivery Charge: Free Shipments:1</div>
                      <button
                        className="border px-md-5 border-dark py-1 fw-light fs-15px bg-white rounded float-end"
                        onClick={() => {
                          this.changeSection("delivery");
                        }}
                      >
                        Change
                      </button>
                    </>
                  )}
                </div>
                {this.state.currentSection === "delivery" && (
                  <div className="p-3 border-top">
                    <div>
                      <span className="fw-bold">Shipment: 1</span> <span>|</span> <span className="text-secondary fs-15px">Standard Delivery</span>
                      <p className="fs-15px text-secondary mt-2">
                        Delivery Charge: <span className="fw-bold text-dark">Rs 0</span>
                      </p>
                      <div className="row">
                        {[...this.props.cart.values()].map((item) => {
                          console.log(item);
                          return <img src={item.image} className="col-2 col-md-1" alt="item"></img>;
                        })}
                      </div>
                      <p className="mt-4 fw-bolder fs-15px">Delivery Slot</p>
                      <select class="form-select" aria-label="Default select example">
                        <option value="1">19 Nov,Sat - 7:00 PM - 9:00 AM</option>
                        <option value="2">20 Nov,Sun - 7:00 AM - 9:00 AM </option>
                      </select>
                      <div className="d-flex justify-content-end mt-4">
                        <button
                          className="col-9 col-md-6 col-lg-4 p-1 border apple-green apple-green-dark-text fw-bold"
                          onClick={() => {
                            this.changeSection("payment");
                          }}
                        >
                          Continue to payments
                        </button>
                      </div>
                    </div>
                  </div>
                )}
              </div>
              <div className="border mt-3">
                <div className="d-flex align-items-center p-2 gap-1 text-secondary">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="17" class="DeliveryOptions___StyledCalendarIcon-sc-z3tst0-2 hbKlct">
                    <path
                      fill-rule="evenodd"
                      d="M15.137 4.33H.847a.155.155 0 0 1-.155-.156v-.947c0-.086.07-.155.156-.155h14.289c.086 0 .155.07.155.155v.947c0 .086-.07.155-.155.155m0 11.479H.847a.155.155 0 0 1-.155-.156V5.16c0-.086.07-.155.156-.155h14.289c.086 0 .155.07.155.155v10.492c0 .086-.07.156-.155.156M12.55 2.224V.811A.31.31 0 0 0 12.24.5h-.124a.31.31 0 0 0-.311.31v1.414c0 .086-.07.156-.156.156H8.575a.155.155 0 0 1-.156-.156V.811A.31.31 0 0 0 8.11.5h-.125a.31.31 0 0 0-.31.31v1.414c0 .086-.07.156-.156.156H4.412a.155.155 0 0 1-.156-.156V.811A.31.31 0 0 0 3.946.5H3.82a.31.31 0 0 0-.31.31v1.414c0 .086-.07.156-.156.156H.311A.31.31 0 0 0 0 2.69v13.5c0 .17.14.31.31.31h15.364a.31.31 0 0 0 .31-.31V2.69a.31.31 0 0 0-.31-.31h-2.967a.155.155 0 0 1-.156-.156M8.94 13.735H6.92a.155.155 0 0 1-.155-.155v-2.02c0-.086.07-.155.155-.155h2.02c.086 0 .155.07.155.155v2.02c0 .085-.07.155-.155.155m-2.858-2.703v3.076c0 .171.139.31.31.31h3.076a.31.31 0 0 0 .31-.31v-3.076a.31.31 0 0 0-.31-.31H6.392a.31.31 0 0 0-.31.31m-1.593 2.687H2.47a.155.155 0 0 1-.154-.155v-2.02c0-.085.07-.155.155-.155h2.02c.085 0 .155.07.155.156v2.02c0 .085-.07.154-.156.154m-2.858-2.702v3.075c0 .172.14.31.31.31h3.076a.31.31 0 0 0 .311-.31v-3.075a.31.31 0 0 0-.31-.311H1.941a.31.31 0 0 0-.31.31M13.39 9.293h-2.02a.155.155 0 0 1-.154-.155v-2.02c0-.085.07-.155.155-.155h2.02c.085 0 .155.07.155.155v2.02c0 .086-.07.155-.156.155M10.532 6.59v3.076c0 .172.14.31.31.31h3.076a.31.31 0 0 0 .311-.31V6.589a.31.31 0 0 0-.31-.31h-3.076a.31.31 0 0 0-.311.31M8.94 9.292H6.92a.155.155 0 0 1-.155-.155v-2.02c0-.085.07-.155.155-.155h2.02c.086 0 .155.07.155.155v2.02c0 .086-.07.155-.155.155M6.082 6.59v3.076c0 .172.139.31.31.31h3.076a.31.31 0 0 0 .31-.31V6.589a.31.31 0 0 0-.31-.31H6.392a.31.31 0 0 0-.31.31M4.489 9.277H2.47a.155.155 0 0 1-.154-.156v-2.02c0-.085.07-.154.155-.154h2.02c.085 0 .155.07.155.155v2.02c0 .085-.07.155-.156.155M1.631 6.574V9.65c0 .171.14.31.31.31h3.076a.31.31 0 0 0 .311-.31V6.574a.31.31 0 0 0-.31-.31H1.941a.31.31 0 0 0-.31.31"
                    ></path>
                  </svg>
                  <p className={"m-0" + (this.state.currentSection === "payment" ? " fw-bold fs-5" : "")}>Payment Options</p>
                </div>
                {this.state.currentSection === "payment" && (
                  <div className="row m-0 border-top">
                    <div className="col-12 col-md-3 p-0 border-end d-flex d-md-block payment-options">
                      <p className="border-bottom m-0 py-3 px-4 bg-light text-nowrap" role="button">
                        Credit / Debit Card
                      </p>
                      <p className="border-bottom m-0 py-3 px-4" role="button">
                        Netbanking
                      </p>
                      <p className="border-bottom m-0 py-3 px-4" role="button">
                        UPI
                      </p>
                      <p className="border-bottom m-0 py-3 px-4" role="button">
                        Wallet
                      </p>
                      <p className="border-bottom m-0 py-3 px-4 text-nowrap" role="button">
                        Pay Later
                      </p>
                      <p className="m-0 py-3 px-4 text-nowrap" role="button">
                        Cash on Delivery
                      </p>
                    </div>
                    <div className="col-md-9 col-12 bg-light">
                      <div className="bg-white mt-3 mb-3 mb-md-0 px-md-5 px-3 py-3 border ">
                        <p className="fw-bold fs-6">Add Credit / Debit card</p>
                        <div className="col-md-8 col-12">
                          <div className="d-flex flex-column">
                            <label className="fs-10px">Card Number</label>
                            <input
                              type="number"
                              className="border-0 border-bottom fs-15px"
                              placeholder="Enter card number"
                              value={this.state.creditCard.number}
                              onChange={this.handleCardDetails}
                              name="number"
                            ></input>
                          </div>
                          {this.state.creditCardError.number && <p className="fs-12px m-0 text-danger">Enter valid card number</p>}

                          <div className="row mt-3 m-0 p-0">
                            <div className="d-flex flex-column m-0 p-0 col-5">
                              <label className="fs-10px ">Valid Thru</label>
                              <input
                                className="border-0 border-bottom fs-15px"
                                placeholder="MM / YY"
                                value={this.state.creditCard.valid}
                                onChange={this.handleCardDetails}
                                name="valid"
                              ></input>
                              {this.state.creditCardError.valid && <p className="fs-12px m-0 text-danger">Enter valid Date</p>}
                            </div>

                            <div className="d-flex flex-column col-5 offset-2 p-0">
                              <label className="fs-10px">CVV</label>
                              <input
                                className="border-0 border-bottom fs-15px"
                                placeholder="Security code"
                                type="password"
                                value={this.state.creditCard.cvv}
                                onChange={this.handleCardDetails}
                                name="cvv"
                              ></input>
                              {this.state.creditCardError.cvv && <p className="fs-12px m-0 text-danger">Enter valid CVV</p>}
                            </div>
                          </div>
                          <div className="col-12 d-flex justify-content-end mt-4">
                            <button className="mt-4 border apple-green apple-green-dark-text fw-bold px-5 py-2" onClick={this.submitCreditCard}>
                              Place order & Pay
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            <div className="col-md-3 col-12 mt-3 mt-md-0 d-none d-md-block">
              <p className="fw-bold">Order Summary</p>
              <div className="order-border">
                <div className="border px-2 py-2">
                  <div className="d-flex justify-content-between fs-15px">
                    <span className="d-block">Basket value</span>
                    <span className="">Rs {totalPrice}</span>
                  </div>
                  <div className="d-flex justify-content-between mt-2 fs-15px">
                    <span className="d-block fw-bold">Total Amount Payable</span>
                    <span className="fw-bold">Rs {totalPrice}</span>
                  </div>
                  <div className="d-flex justify-content-between fs-15px mt-2 border p-2 apple-green apple-green-dark-text fw-bold">
                    <span className="d-block">Total Savings</span>
                    <span>Rs {(totalPrice * ((Math.random() + Math.random() + 8) / 100)).toFixed(2)}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="w-100 container mt-5 apple-green">
          <div className="d-flex flex-column align-items-center m-auto p-5 border rounded-2">
            <h5 className="text-center">Your order is placed successfully</h5>
          </div>
        </div>
      );
    }
  }
}

export default Checkout;
