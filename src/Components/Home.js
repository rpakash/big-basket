import React from "react";
import HotSips from "../images/hot-sips.webp";
import ProductCard from "./ProductCard";
import { Link } from "react-router-dom";

class Home extends React.Component {
  render() {
    return (
      <div className="container mt-5">
        <div className="border rounded-4 p-4 background-gray">
          <p className="fs-4 fw-bold">Best Sellers</p>

          <div className="row m-0">
            {this.props.products.slice(0, 4).map((product) => {
              return (
                <div className="col-12 col-md-6 col-lg-4 col-xl-3 col-sm-12 py-2" key={product.name}>
                  <ProductCard
                    product={product}
                    cart={this.props.cart}
                    addProduct={this.props.addProduct}
                    decreaseProductQuantity={this.props.decreaseProductQuantity}
                  />
                </div>
              );
            })}
          </div>
        </div>
        <div className="mt-5">
          <p className="fs-4 fw-bold">Fruits & Vegetables</p>
          <div className="d-flex flex-wrap gap-md-2 gap-1 justify-content-center">
            <div className="border col-md col-sm col-5 shadow rounded">
              <Link to={"/category/fruits"}>
                <img
                  className="w-100 rounded card-hover rounded"
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/9801292b-8291-488a-bb4e-7712022dc060/hp_fresh-fruits-fnv_Storefront_m_251022_02.jpg?tr=w-1920,q=80"
                  alt="fruit"
                ></img>
              </Link>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow rounded shadow">
              <Link to={"/category/vegetables"}>
                <img
                  className="w-100 rounded rounded"
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/9801292b-8291-488a-bb4e-7712022dc060/hp_fresh-veggs-fnv_Storefront_m_251022_03.jpg?tr=w-1920,q=80"
                  alt="fruit"
                ></img>
              </Link>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow rounded shadow">
              <Link to={"/category/cuts-sprouts"}>
                <img
                  className="w-100 rounded rounded"
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/9801292b-8291-488a-bb4e-7712022dc060/hp_cutx-sprouts-fnv_Storefront_m_251022_04.jpg?tr=w-1920,q=80"
                  alt="fruit"
                ></img>
              </Link>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow rounded shadow">
              <Link to={"/category/exotic-fruits-veggies"}>
                <img
                  className="w-100 rounded rounded"
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/9801292b-8291-488a-bb4e-7712022dc060/hp_exotic-fnv_Storefront_m_251022_05.jpg?tr=w-1920,q=80"
                  alt="fruit"
                ></img>
              </Link>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow rounded shadow">
              <Link to={"/category/herbs-seasonings"}>
                <img
                  className="w-100 rounded rounded"
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/9801292b-8291-488a-bb4e-7712022dc060/hp_herbs-seasoning-fnv_m_251022_06.jpg?tr=w-1920,q=80"
                  alt="fruit"
                ></img>
              </Link>
            </div>
            <div className="col-md col-sm col-5 border card-hover rounded shadow">
              <img
                className="w-100 rounded rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/9801292b-8291-488a-bb4e-7712022dc060/hp_organic-fnv_Storefront_m_251022_01.jpg?tr=w-1920,q=80"
                alt="vege"
              ></img>
            </div>
          </div>
        </div>
        <div className="mt-5">
          <p className="fs-4 fw-bold">Your Daily Staples</p>
          <div className="d-flex flex-wrap gap-md-2 gap-1 justify-content-center">
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/fdc267dd-d968-4b49-933f-9f007b7aab7d/hp_atta-flour-staplesStorefront_m_480_251022_01.jpg?tr=w-1920,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/fdc267dd-d968-4b49-933f-9f007b7aab7d/hp_rice-staplesStorefront_m_480_251022_02.jpg?tr=w-1920,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/fdc267dd-d968-4b49-933f-9f007b7aab7d/hp_dals-pulses-staplesStorefront_m_480_251022_03.jpg?tr=w-1920,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/fdc267dd-d968-4b49-933f-9f007b7aab7d/hp_cooking-oil-staplesStorefront_m_480_251022_04.jpg?tr=w-1920,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/fdc267dd-d968-4b49-933f-9f007b7aab7d/hp_dry-fruits-staplesStorefront_m_480_251022_05.jpg?tr=w-1920,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="col-md col-sm col-5 border card-hover shadow rounded">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/fdc267dd-d968-4b49-933f-9f007b7aab7d/hp_salt-sugar-staplesStorefront_m_480_251022_06.jpg?tr=w-1920,q=80"
                alt="fruit"
              ></img>
            </div>
          </div>
        </div>
        <div className="mt-5">
          <p className="fw-bold fs-4">Beverages</p>
          <div className="row gap-3 m-0 p-0">
            <div className="col-12 col-md border card-hover shadow rounded">
              <img src={HotSips} alt="hot-sips"></img>
            </div>
            <div className="col-12 col-md p-0">
              <div className="d-flex m-0 p-0 gap-2 justify-content-center">
                <img
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/c9120807-7db5-4f39-9895-3b03dee91c54/hp_coost-beveragesStorefront_m_251022_275x184_02.jpg?tr=w-1920,q=80"
                  alt="drinks"
                  className="col-6 border  card-hover rounded shadow"
                ></img>
                <img
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/c9120807-7db5-4f39-9895-3b03dee91c54/hp_coffee-beveragesStorefront_m_251022_275x184_03.jpg?tr=w-1920,q=80"
                  alt="drinks"
                  className="col-6 border  card-hover rounded shadow"
                ></img>
              </div>
              <div className="d-flex gap-2 mt-2 p-0 align-items-center justify-content-center">
                <img
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/c9120807-7db5-4f39-9895-3b03dee91c54/hp_health-beveragesStorefront_m_251022_275x184_04.jpg?tr=w-1920,q=80"
                  alt="drinks"
                  className="col-6 border  card-hover rounded shadow"
                ></img>
                <img
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/c9120807-7db5-4f39-9895-3b03dee91c54/hp_energy-beveragesStorefront_m_251022_275x184_05.jpg?tr=w-1920,q=80"
                  alt="drinks"
                  className="col-6 border  card-hover rounded shadow"
                ></img>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-5">
          <p className="fs-4 fw-bold">Snack Store</p>
          <div className="d-flex flex-wrap gap-md-2 gap-1 justify-content-center">
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/307d4a8d-5ffb-43c3-a70e-59d26c671cc2/hp_namkeens-snacksStorefront_m_480_251022_01.jpg?tr=w-1080,q=80"
                alt="snack"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/307d4a8d-5ffb-43c3-a70e-59d26c671cc2/hp_frozen-snacks-snacksStorefront_m_480_251022_02.jpg?tr=w-1080,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/307d4a8d-5ffb-43c3-a70e-59d26c671cc2/hp_ready-cook-snacksStorefront_m_480_251022_04.jpg?tr=w-1080,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/307d4a8d-5ffb-43c3-a70e-59d26c671cc2/hp_soups-noodles-snacksStorefront_m_480_251022_03.jpg?tr=w-1080,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/307d4a8d-5ffb-43c3-a70e-59d26c671cc2/hp_biscuit-cookies-snacksStorefront_m_480_251022_05.jpg?tr=w-1080,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="col-md col-sm col-5 border card-hover">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/307d4a8d-5ffb-43c3-a70e-59d26c671cc2/hp_chocolates-snacksStorefront_m_480_251022_06-13.jpg?tr=w-1080,q=80"
                alt="fruit"
              ></img>
            </div>
          </div>
        </div>
        <div className="mt-5">
          <p className="fs-4 fw-bold">Cleaning & Household</p>
          <div className="d-flex flex-wrap gap-md-2 gap-1 justify-content-center">
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/d3dc58d9-5531-4b8c-ae8c-740a6f4dce51/hp_cleaners-disnfectants_m_480_251022_01.jpg?tr=w-1080,q=80"
                alt="snack"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/d3dc58d9-5531-4b8c-ae8c-740a6f4dce51/hp_detetgents-cleaningStorefront_m_480_251022_02.jpg?tr=w-1080,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/d3dc58d9-5531-4b8c-ae8c-740a6f4dce51/hp_kitchen-wipes-m_480_251022_03.jpg?tr=w-1080,q=80"
                alt="fruit"
              ></img>
            </div>
            <div className="border col-md col-sm col-5 card-hover rounded shadow">
              <img
                className="w-100 rounded"
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/d3dc58d9-5531-4b8c-ae8c-740a6f4dce51/hp_fresheners-repplent-_m_480_251022_04.jpg?tr=w-1080,q=80"
                alt="fruit"
              ></img>
            </div>
          </div>
        </div>
        <div className="mt-5">
          <p className="fw-bold fs-4">Beauty & Hygiene</p>
          <div className="row gap-2 m-0">
            <div className="col-12 col-md border card-hover shadow rounded">
              <img
                src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/6fe7b988-d217-4451-a8ff-6ddd2762f3d5/hp_beauty-Storefront_m_251022_560x378_01.jpg?tr=w-1080,q=80"
                alt="hot-sips"
              ></img>
            </div>
            <div className="col-12 col-md p-0">
              <div className="d-flex m-0 p-0 gap-2">
                <img
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/6fe7b988-d217-4451-a8ff-6ddd2762f3d5/hp_beauty-Storefront_m_251022_275x184_02.jpg?tr=w-1080,q=80"
                  alt="drinks"
                  className="col-6 border card-hover rounded shadow"
                ></img>
                <img
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/6fe7b988-d217-4451-a8ff-6ddd2762f3d5/hp_beauty-Storefront_m_251022_275x184_03.jpg?tr=w-1080,q=80"
                  alt="drinks"
                  className="col-6 border card-hover rounded shadow"
                ></img>
              </div>
              <div className="d-flex gap-2 mt-2">
                <img
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/6fe7b988-d217-4451-a8ff-6ddd2762f3d5/hp_beauty-Storefront_m_251022_275x184_04.jpg?tr=w-1920,q=80"
                  alt="drinks"
                  className="col-6 border card-hover rounded shadow"
                ></img>
                <img
                  src="https://www.bigbasket.com/media/customPage/77880b23-0233-4fad-b54a-a93c998e0d20/7f6ddfd5-93af-4bbd-a250-ce828daa63d6/6fe7b988-d217-4451-a8ff-6ddd2762f3d5/hp_beauty-Storefront_m_251022_275x184_05.jpg?tr=w-1920,q=80"
                  alt="drinks"
                  className="col-6 border card-hover rounded shadow"
                ></img>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
