import React from "react";
import Basket from "../images/basket.svg";
import { Link } from "react-router-dom";

class Cart extends React.Component {
  goToCheckOut = () => {
    if (this.props.isLogged) {
      this.props.history.push("/checkout");
    } else {
      this.props.showAuth("/checkout");
    }
  };

  render() {
    let totalPrice = [...this.props.cart].reduce((acc, [key, item]) => {
      return acc + item.quantity * item.price.price;
    }, 0);

    if (this.props.cart.size > 0) {
      return (
        <div className="container">
          <p className="mt-3 fw-bold fs-5">Your Basket</p>
          <div className="bg-dark text-white rounded row p-2 align-items-center d-none d-md-flex">
            <div className="col">
              <p className="m-0">
                <span className="fw-bold">Subtotal</span> ({this.props.cart.size} items): <span className="fw-bold">₹{totalPrice.toFixed(2)}</span>
              </p>
              <p className="m-0 text-success">
                Savings: <span className="fw-bolder">₹{(totalPrice * ((Math.random() + Math.random() + 8) / 100)).toFixed(2)}</span>
              </p>
            </div>
            <div className="col">
              <button className="bg-danger border-0 px-5 py-1 text-white rounded float-end" onClick={this.goToCheckOut}>
                Checkout
              </button>
            </div>
          </div>
          <div className="bg-dark text-white rounded row p-2 align-items-center fixed-bottom d-md-none">
            <div className="col">
              <p className="m-0">
                <span className="fw-bold">₹{totalPrice.toFixed(2)}</span>
              </p>
              <p className="m-0 text-success">
                Saved <span className="fw-bolder">₹{(totalPrice * ((Math.random() + Math.random() + 8) / 100)).toFixed(2)}</span>
              </p>
            </div>
            <div className="col">
              <button className="bg-danger border-0 px-3 py-1 text-white rounded float-end" onClick={this.goToCheckOut}>
                Checkout
              </button>
            </div>
          </div>
          <div className="row gap-2 mt-md-4">
            <div className="row text-secondary d-none d-md-flex">
              <div className="col-6">
                <p>Items (2 items)</p>
              </div>
              <div className="col-3">
                <p className="text-center">Quantity</p>
              </div>
              <div className="col-3 text-end">
                <p>Sub-total</p>
              </div>
            </div>

            {[...this.props.cart].map(([key, item]) => {
              return (
                <div className="row border-bottom align-items-center m-0">
                  <img src={item.image} alt="product" className="col-3 col-md-2 d-inline-block"></img>
                  <div className="row col-md-10 p-0 col-9 align-items-center">
                    <div className="col-12 col-md-4">
                      <p>
                        {item.brand} {item.name}, {item.price.quantity}Kg
                      </p>
                      <p className="d-none d-md-block">₹{item.price.price}</p>
                    </div>
                    <div className="col-6 col-md-4 m-md-auto d-md-flex flex-md-column justify-content-center align-items-center p-0">
                      <div className="border rounded p-md-2 p-1 ms-4">
                        <div className="justify-content-between d-flex align-items-center">
                          <div
                            className="d-flex justify-content-center align-items-center sign-hover sign p-1"
                            role={"button"}
                            onClick={() => {
                              this.props.decreaseProductQuantity(`${item.name}#${item.price.price}`);
                            }}
                          >
                            <i className="minus-svg "></i>
                          </div>
                          <span className="px-2">{this.props.cart.get(`${item.name}#${item.price.price}`)["quantity"]}</span>
                          <div
                            className="d-flex justify-content-center align-items-center sign-hover sign py-1"
                            role={"button"}
                            onClick={() => this.props.addProduct(`${item.name}#${item.price.price}`)}
                          >
                            <i className="plus-svg "></i>
                          </div>
                        </div>
                      </div>
                      <p className="cart-delete m-2 ms-4" role="button" onClick={() => this.props.removeProduct(`${item.name}#${item.price.price}`)}>
                        Delete
                      </p>
                    </div>
                    <div className="col-6 col-md-3 text-end">
                      <p>₹{(item.quantity * item.price.price).toFixed(2)}</p>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      );
    } else {
      return (
        <div className="w-100 container mt-5 apple-green">
          <div className="d-flex flex-column align-items-center m-auto p-5 border rounded-2">
            <img src={Basket} className="noCart" alt="no-cart"></img>
            <h5 className="text-center">
              Let's fill the empty <span>Basket</span>
            </h5>
            <Link to={"/"}>
              <button className="border-0 bg-danger p-2 rounded text-white">Continue Shopping</button>
            </Link>
          </div>
        </div>
      );
    }
  }
}

export default Cart;
