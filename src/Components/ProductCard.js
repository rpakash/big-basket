import React from "react";
import { Link } from "react-router-dom";

class ProductCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      price: props.product.price[0],
    };
  }

  handleSelect = (event) => {
    let priceArray = event.target.value.split("#");
    this.setState({
      price: { price: priceArray[0], quantity: priceArray[1] },
    });
  };

  render() {
    return (
      <div className="col border-0 rounded shadow p-3 bg-white d-block card-hover">
        <div className="d-sm-flex d-flex d-md-block gap-2">
          <img src={this.props.product.image} alt="one" className="m-auto d-block border rounded col-md-12 col-4" />
          <div className="col-8 col-md-12">
            <div className="align-items-center justify-content-end mt-1 d-none d-md-flex">
              <div className="border rounded px-1 delivery-time">
                <i className="bike-svg"></i>
                <span className="fs-10px">3hrs</span>
              </div>
            </div>
            <div className="p-1 pb-3">
              <p className="product-category mb-0">{this.props.product.brand}</p>
              <Link to={`/product/${this.props.product.name}`}>
                <p className="product-title">{this.props.product.name}</p>
              </Link>
              <div className="">
                {this.props.product.ratings && (
                  <div className="fs-12px d-flex align-items-center">
                    <div className="rounded d-flex align-items-center justify-content-center ps-1 rating">
                      <span>4.4 </span>
                      <i className="star-svg"></i>
                    </div>
                    <span className="ms-1">3159 Ratings</span>
                  </div>
                )}
                <select className="form-select" onChange={this.handleSelect}>
                  {this.props.product.price.map((price) => {
                    return <option key={price.price} value={price.price + "#" + price.quantity}>{`${price.quantity} Kg`}</option>;
                  })}
                </select>
                <div className="mt-2">
                  <span className="fw-bold price">₹{this.state.price.price}</span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="d-flex m-auto mt-2 gap-2">
          <button className="d-flex justify-content-center border border-1 border-dark bg-white py-2 rounded">
            <i className="bookmark-svg d-block"></i>
          </button>
          {!this.props.cart.has(`${this.props.product.name}#${this.state.price.price}`) && (
            <button
              className="col-5 border bg-white flex-grow-1 text-danger rounded border-1 border-danger add-hover"
              onClick={() =>
                this.props.addProduct(`${this.props.product.name}#${this.state.price.price}`, {
                  ...this.props.product,
                  price: this.state.price,
                })
              }
            >
              Add
            </button>
          )}
          {this.props.cart.has(`${this.props.product.name}#${this.state.price.price}`) && (
            <div className="justify-content-between d-flex align-items-center rounded border w-100">
              <div
                className="d-flex rounded justify-content-center align-items-center sign bg-danger p-2"
                role={"button"}
                onClick={() => {
                  this.props.decreaseProductQuantity(`${this.props.product.name}#${this.state.price.price}`);
                }}
              >
                <i className="minus-svg "></i>
              </div>
              <span>{this.props.cart.get(`${this.props.product.name}#${this.state.price.price}`)["quantity"]}</span>
              <div
                className="d-flex rounded justify-content-center align-items-center sign p-2 bg-danger"
                role={"button"}
                onClick={() => this.props.addProduct(`${this.props.product.name}#${this.state.price.price}`)}
              >
                <i className="plus-svg "></i>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default ProductCard;
