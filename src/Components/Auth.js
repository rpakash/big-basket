import React from "react";
import validator from "validator";

class Auth extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetails: {},
      stage: 1,
      error: {
        id: "",
      },
    };
    this.isError = false;
  }
  handleContinue = (event) => {
    this.setState(
      {
        invalidCredentials: false,
        error: {},
        isError: false,
      },
      () => {
        this.isError = false;
        event.preventDefault();
        let users = localStorage.getItem("users");

        if (this.state.stage === 1) {
          if (!validator.isEmail(this.state.userDetails.id || "")) {
            this.setState((state) => ({
              error: { ...state.error, id: "Enter vaild email" },
            }));
            this.isError = true;
          }

          if (
            !validator.isLength(this.state.userDetails.password || "", {
              min: 8,
            })
          ) {
            this.setState((state) => ({
              error: { ...state.error, password: "Password length should be atleast 8" },
            }));
            this.isError = true;
          }

          if (this.isError) {
            return;
          }

          if (!users) {
            users = {};
          } else {
            users = JSON.parse(users);
          }

          if (users[this.state.userDetails.id]) {
            if (users[this.state.userDetails.id].password !== this.state.userDetails.password) {
              console.log(users[this.state.userDetails.id].password, this.state.userDetails.password);
              this.setState({
                invalidCredentials: true,
              });
            } else {
              this.props.setUser(users[this.state.userDetails.id]);
              localStorage.setItem("currentUser", JSON.stringify(users[this.state.userDetails.id]));
            }
          } else {
            this.setState({
              stage: 2,
            });
          }
        } else if (this.state.stage === 2) {
          if (!users) {
            users = {};
          } else {
            users = JSON.parse(users);
          }

          if (!validator.isAlpha(this.state.userDetails.firstName?.trim() || "")) {
            this.setState((state) => ({
              error: { ...state.error, fullName: "First name is required " },
            }));
            this.isError = true;
          }

          if (!validator.isAlpha(this.state.userDetails.lastName?.trim() || "")) {
            this.setState((state) => ({
              error: { ...state.error, lastName: "Last name is required" },
            }));
            this.isError = true;
          }

          if (this.isError) {
            return;
          }

          users[this.state.userDetails.id] = this.state.userDetails;
          localStorage.setItem("users", JSON.stringify(users));
          localStorage.setItem("currentUser", JSON.stringify(users[this.state.userDetails.id]));
          this.props.setUser(this.state.userDetails);
        }
      }
    );
  };

  handleChange = (event) => {
    this.setState((state) => {
      return {
        userDetails: { ...state.userDetails, [event.target.name]: event.target.value },
      };
    });
  };

  render() {
    return (
      <div className="popupbg">
        <div className="popup">
          <div className="d-flex justify-content-between">
            <div></div>
            <h5 className="apple-green-dark-text">Login / Sign up</h5>
            <i className="cross" onClick={this.props.close} role="button"></i>
          </div>
          <form>
            {this.state.stage === 1 && (
              <>
                <div className="mb-3 mt-5">
                  <input type="email" className="border-bottom border-0" id="email" placeholder="Email" name="id" onChange={this.handleChange} />
                  {this.state.error.id !== "" && <p className="text-danger">{this.state.error.id}</p>}
                </div>
                <div className="mb-3">
                  <input
                    type="password"
                    className="border-bottom border-0"
                    id="password"
                    name="password"
                    placeholder="Password"
                    onChange={this.handleChange}
                  />
                  {this.state.error.password !== "" && <p className="text-danger">{this.state.error.password}</p>}
                </div>
              </>
            )}
            {this.state.stage === 2 && (
              <>
                <div className="mb-3 mt-5">
                  <input
                    type="text"
                    className="border-bottom border-0"
                    id="first-name"
                    placeholder="First Name"
                    name="firstName"
                    onChange={this.handleChange}
                  />
                  {this.state.error.fullName !== "" && <p className="text-danger">{this.state.error.fullName}</p>}
                </div>
                <div className="mb-3">
                  <input
                    type="text"
                    className="border-bottom border-0"
                    id="last-name"
                    name="lastName"
                    placeholder="Last Name"
                    onChange={this.handleChange}
                  />
                  {this.state.error.lastName !== "" && <p className="text-danger">{this.state.error.lastName}</p>}
                </div>
              </>
            )}
            {this.state.invalidCredentials && <p className="text-danger">Incorrect Email/Password</p>}
            <button type="submit" className="apple-green-dark text-white border-0 mt-2 w-100" onClick={this.handleContinue}>
              Continue
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Auth;
