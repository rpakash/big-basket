import ProductCard from "./ProductCard";
import React from "react";
import Filter from "./Filter";

class Category extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: {
        price: [],
        brand: [],
        sort: "rele",
      },
    };
  }

  setFilter = (filter) => {
    this.setState((state) => {
      return {
        filter: { ...state.filter, ...filter },
      };
    });
  };

  sortProduct = (event) => {
    this.setState((state) => ({
      filter: { ...state.filter, sort: event.target.value },
    }));
  };

  render() {
    let products = [...this.props.products]
      .filter((product) => {
        return product.type.toLowerCase() === this.props.match.params.category;
      })
      .filter((product) => {
        if (
          this.state.filter.price.length > 0 &&
          !this.state.filter.price.some((element) => {
            console.log(product.price[0].price, element[0], product.price[0].price, element[1]);
            return product.price[0].price >= element[0] && product.price[0].price <= element[1];
          })
        ) {
          return false;
        }

        if (this.state.filter.brand.length > 0 && !this.state.filter.brand.includes(product.brand)) {
          return false;
        }
        return true;
      })
      .sort((productA, productB) => {
        if (this.state.filter.sort === "rele") {
          return 0;
        } else if (this.state.filter.sort === "asc") {
          return productA.price[0].price - productB.price[0].price;
        } else {
          return productB.price[0].price - productA.price[0].price;
        }
      });
    return (
      <>
        <div className="container mt-5 bg-gray">
          <p className="text-capitalize fs-3 fw-bold">{this.props.match.params.category.split("-").join(" ")}</p>
          <div className="row">
            <div className="col-3 gy-4 d-sm-none d-none d-md-block">
              <Filter products={products} setFilter={this.setFilter} />
            </div>
            <div className="row gy-2 col-md-9 col-12 m-0">
              <div className="w-100">
                <div className="d-flex col-5 col-lg-5 col-xl-4 col-md-6 align-items-center float-end  d-none d-md-flex">
                  <svg width="32" height="22" xmlns="http://www.w3.org/2000/svg" class="ml-1.5 fill-current me-2">
                    <path
                      fill-rule="evenodd"
                      d="M10.583 17.417a.917.917 0 1 1 0-1.834.917.917 0 0 1 0 1.834Zm9.167-1.834h-6.587a2.745 2.745 0 0 0-2.58-1.833c-1.193 0-2.2.77-2.58 1.833H3.25a.916.916 0 1 0 0 1.834h4.753a2.745 2.745 0 0 0 2.58 1.833c1.194 0 2.2-.77 2.58-1.833h6.587a.916.916 0 1 0 0-1.834Zm-1.834-3.666a.917.917 0 1 1 .001-1.834.917.917 0 0 1 0 1.834Zm0-3.667c-1.193 0-2.2.77-2.58 1.833H3.25a.916.916 0 1 0 0 1.834h12.086a2.745 2.745 0 0 0 2.58 1.833 2.753 2.753 0 0 0 2.75-2.75 2.753 2.753 0 0 0-2.75-2.75Zm-11-3.667a.917.917 0 1 1 0 1.834.917.917 0 0 1 0-1.834ZM3.25 6.417h1.086a2.745 2.745 0 0 0 2.58 1.833c1.194 0 2.201-.77 2.58-1.833H19.75a.916.916 0 1 0 0-1.834H9.497a2.745 2.745 0 0 0-2.58-1.833c-1.194 0-2.202.77-2.581 1.833H3.25a.916.916 0 1 0 0 1.834Z"
                    ></path>
                  </svg>
                  <select className="form-select ps-2" onChange={this.sortProduct}>
                    <option value="rele">Relevance</option>
                    <option value="asc">Price - Low to High</option>
                    <option value="desc">Price - High to Low</option>
                  </select>
                </div>
                <div className="d-flex justify-content-end gap-2 d-md-none">
                  <button
                    className="border fs-15px px-3 py-1"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasSort"
                    aria-controls="offcanvasSort"
                  >
                    <svg width="32" height="22" xmlns="http://www.w3.org/2000/svg" class="ml-1.5 fill-current me-2">
                      <path
                        fill-rule="evenodd"
                        d="M10.583 17.417a.917.917 0 1 1 0-1.834.917.917 0 0 1 0 1.834Zm9.167-1.834h-6.587a2.745 2.745 0 0 0-2.58-1.833c-1.193 0-2.2.77-2.58 1.833H3.25a.916.916 0 1 0 0 1.834h4.753a2.745 2.745 0 0 0 2.58 1.833c1.194 0 2.2-.77 2.58-1.833h6.587a.916.916 0 1 0 0-1.834Zm-1.834-3.666a.917.917 0 1 1 .001-1.834.917.917 0 0 1 0 1.834Zm0-3.667c-1.193 0-2.2.77-2.58 1.833H3.25a.916.916 0 1 0 0 1.834h12.086a2.745 2.745 0 0 0 2.58 1.833 2.753 2.753 0 0 0 2.75-2.75 2.753 2.753 0 0 0-2.75-2.75Zm-11-3.667a.917.917 0 1 1 0 1.834.917.917 0 0 1 0-1.834ZM3.25 6.417h1.086a2.745 2.745 0 0 0 2.58 1.833c1.194 0 2.201-.77 2.58-1.833H19.75a.916.916 0 1 0 0-1.834H9.497a2.745 2.745 0 0 0-2.58-1.833c-1.194 0-2.202.77-2.581 1.833H3.25a.916.916 0 1 0 0 1.834Z"
                      ></path>
                    </svg>
                    Sort
                  </button>
                  <div class="offcanvas offcanvas-end w-50" tabindex="-1" id="offcanvasSort" aria-labelledby="offcanvasRightLabel">
                    <div class="offcanvas-header">
                      <h5 id="offcanvasRightLabel">Sort</h5>
                      <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                      <p
                        role="button"
                        data-bs-toggle="offcanvas"
                        data-bs-dismiss="offcanvas"
                        onClick={() => this.sortProduct({ target: { value: "rele" } })}
                        className={this.state.filter.sort === "rele" ? "apple-green-dark-text" : ""}
                      >
                        Relevance
                      </p>
                      <p
                        role="button"
                        data-bs-toggle="offcanvas"
                        data-bs-dismiss="offcanvas"
                        onClick={() => this.sortProduct({ target: { value: "asc" } })}
                        className={this.state.filter.sort === "asc" ? "apple-green-dark-text" : ""}
                      >
                        Price - Low to High
                      </p>
                      <p
                        role="button"
                        data-bs-toggle="offcanvas"
                        data-bs-dismiss="offcanvas"
                        onClick={() => this.sortProduct({ target: { value: "desc" } })}
                        className={this.state.filter.sort === "desc" ? "apple-green-dark-text" : ""}
                      >
                        hover:underline hover:underline hover:underline hover:underline hover:underline hover:underline hover:underline
                        hover:underline hover:underline hover:underline hover:underline hover:underline hover:underline hover:underline
                        hover:underline hover:underline Price - High to Low
                      </p>
                    </div>
                  </div>
                  <button
                    className="border fs-15px px-3 py-1"
                    data-bs-toggle="offcanvas"
                    data-bs-target="#offcanvasFilter"
                    aria-controls="offcanvasFilter"
                  >
                    <svg
                      version="1.1"
                      id="Capa_1"
                      xmlns="http://www.w3.org/2000/svg"
                      x="0px"
                      y="0px"
                      width="32"
                      height="22"
                      viewBox="0 0 485.008 485.008"
                      style={{ enableBackground: "new 0 0 485.008 485.008" }}
                    >
                      <g>
                        <g>
                          <path
                            d="M171.501,464.698v-237.9l-166.3-192.6c-8.9-10.9-7.9-33.3,15.1-33.3h443.6c21.6,0,26.6,19.8,15.1,33.3l-162.3,187.5v147.2
			c0,6-2,11.1-7.1,15.1l-103.8,95.8C193.801,488.698,171.501,483.898,171.501,464.698z M64.701,41.298l142.2,164.3c3,4,5,8.1,5,13.1
			v200.6l64.5-58.5v-146.1c0-5,2-9.1,5-13.1l138.1-160.3L64.701,41.298L64.701,41.298z"
                          />
                        </g>
                      </g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                      <g></g>
                    </svg>
                    Fitler
                  </button>
                  <div class="offcanvas offcanvas-end w-50" tabindex="-1" id="offcanvasFilter" aria-labelledby="offcanvasRightLabel">
                    <div class="offcanvas-header">
                      <h5 id="offcanvasRightLabel">Filter</h5>
                      <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    </div>
                    <div class="offcanvas-body">
                      <Filter products={products} setFilter={this.setFilter} />
                    </div>
                  </div>
                </div>
              </div>

              {products.length > 0 &&
                products.map((product) => (
                  <div className="col-12 col-md-4 col-lg-4 col-sm-6 py-2" key={product.name}>
                    <ProductCard
                      product={product}
                      cart={this.props.cart}
                      addProduct={this.props.addProduct}
                      decreaseProductQuantity={this.props.decreaseProductQuantity}
                    />
                  </div>
                ))}
              {products.length === 0 && <p className="text-center">No matching products found</p>}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Category;
