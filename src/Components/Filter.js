import { Component } from "react";

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratings: false,
      brands: [],
      price: 0,
      discount: false,
      checkedBrands: [],
      checkedPrice: {},
    };
  }
  componentDidMount() {
    this.props.products.forEach((product) => {
      this.setState((state) => {
        let ratings = state.ratings;
        let brands = state.brands;
        let price = state.price;
        let discount = state.discount;

        if (!state.brands.includes(product.brand)) {
          brands.push(product.brand);
        }

        if (product.ratings) {
          ratings = true;
        }
        if (product.discount) {
          discount = true;
        }

        if (+product.price[0].price < 21 && +product.price[0].price <= 50 && price === 0) {
          price = 1;
        }
        if (+product.price[0].price > 50 && +product.price[0].price <= 100 && price <= 1) {
          price = 2;
        }
        if (+product.price[0].price > 100 && +product.price[0].price <= 200 && price <= 2) {
          price = 3;
        }
        if (+product.price[0].price > 200 && +product.price[0].price <= 500 && price <= 3) {
          price = 4;
        }
        if (+product.price[0].price > 500 && price <= 4) {
          price = 5;
        }

        return {
          price,
          ratings,
          brands,
          discount,
        };
      });
    });
  }

  changePriceFilter = (price) => {
    if (this.state.checkedPrice[price]) {
      this.setState((state) => {
        delete state.checkedPrice[price];
        return {
          checkedPrice: { ...state.checkedPrice },
        };
      }, this.changeFilter);
    } else {
      this.setState((state) => {
        console.log(price);
        let priceArray = [];
        if (price == "50") {
          priceArray = [21, 50];
        } else if (price == "100") {
          priceArray = [50, 100];
        } else if (price == "200") {
          priceArray = [100, 200];
        } else if (price == "500") {
          priceArray = [200, 500];
        } else if (price == "501") {
          priceArray = [500, 1000000];
        }

        return {
          checkedPrice: { ...state.checkedPrice, [price]: priceArray },
        };
      }, this.changeFilter);
    }
  };

  changeFilter = () => {
    this.props.setFilter({
      price: Object.values(this.state.checkedPrice),
      brand: this.state.checkedBrands,
    });
  };

  changeBrandsFilter = (brand) => {
    if (this.state.checkedBrands.includes(brand)) {
      let index = this.state.checkedBrands.findIndex((element) => element === brand);
      this.setState(
        (state) => ({
          checkedBrands: [...state.checkedBrands.slice(0, index), ...state.checkedBrands.slice(index + 1)],
        }),
        this.changeFilter
      );
    } else {
      this.setState(
        (state) => ({
          checkedBrands: [...state.checkedBrands, brand],
        }),
        this.changeFilter
      );
    }
  };

  render() {
    return (
      <>
        <p className="fs-6 fw-bold">Refined By</p>
        {this.state.ratings && (
          <div class="accordion" id="filter-options">
            <div class="accordion-item">
              <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                <button
                  class="accordion-button py-2 px-2 text-dark filter-head"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#panelsStayOpen-collapseOne"
                  aria-expanded="true"
                  aria-controls="panelsStayOpen-collapseOne"
                >
                  Product Rating
                </button>
              </h2>
              <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                <div class="accordion-body row gap-2 m-0 p-0 py-3" style={{ backgroundColor: "#f7f7f7" }}>
                  <div className="d-flex gap-2 align-items-stretch">
                    <input type="checkbox" id="rating-5" className="col-1"></input>
                    <label for="rating-5" className="d-flex col-10 gap-2 fill-green">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                    </label>
                  </div>
                  <div className="d-flex gap-2 align-items-stretch">
                    <input type="checkbox" id="rating-5" className="col-1"></input>
                    <label for="rating-5" className="d-flex col-10 gap-2 fill-green">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                    </label>
                  </div>
                  <div className="d-flex gap-2 align-items-stretch">
                    <input type="checkbox" id="rating-5" className="col-1"></input>
                    <label for="rating-5" className="d-flex col-10 gap-2 fill-green">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                    </label>
                  </div>
                  <div className="d-flex gap-2 align-items-stretch">
                    <input type="checkbox" id="rating-5" className="col-1"></input>
                    <label for="rating-5" className="d-flex col-10 gap-2 fill-green">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="rgba(255,160,51)"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="rgba(255,160,51)"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                    </label>
                  </div>
                  <div className="d-flex gap-2 align-items-stretch">
                    <input type="checkbox" id="rating-5" className="col-1"></input>
                    <label for="rating-5" className="d-flex col-10 gap-2 fill-green">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="red"
                        width="15"
                        height="15"
                        viewBox="0 0 26 26"
                        class="FilterByRating___StyledStarIcon-sc-17wxy9s-3 byEYwx"
                      >
                        <path d="M19.72 16.183l.9 5.254a2 2 0 0 1-2.902 2.108l-4.485-2.358a.5.5 0 0 0-.466 0l-4.485 2.358a2 2 0 0 1-2.902-2.108l.9-5.254-3.816-3.72A2 2 0 0 1 3.572 9.05l5.275-.767 2.36-4.78a2 2 0 0 1 3.586 0l2.36 4.78 5.275.767a2 2 0 0 1 1.108 3.411l-3.817 3.721z"></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                      <svg
                        width="14"
                        height="14"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        class="FilterByRating___StyledStarOutlineIcon-sc-17wxy9s-4 fUxRFu"
                      >
                        <path
                          d="M7.24 11.418 7 11.286l-.241.132-3.06 1.68c-.4.219-.555.14-.569.13-.011-.008-.139-.121-.065-.57l.596-3.626.04-.25-.177-.182-2.492-2.535c-.31-.316-.288-.499-.274-.538.013-.04.108-.201.55-.268l3.398-.516.263-.04.114-.24 1.553-3.285C6.833.762 6.998.75 7 .75c0 0 .166.012.363.428l1.553 3.285.114.24.263.04 3.399.516c.442.067.537.227.55.268.013.038.037.22-.275.538L10.475 8.6l-.178.181.041.25.596 3.627c.073.445-.054.56-.066.568-.014.01-.168.09-.568-.129l-3.06-1.679Z"
                          stroke="#B3B3B3"
                        ></path>
                      </svg>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        <div class="accordion mt-2" id="filter-options">
          <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
              <button
                class="accordion-button py-2 px-2 text-dark filter-head"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#panelsStayOpen-collapseTwo"
                aria-expanded="true"
                aria-controls="panelsStayOpen-collapseTwo"
              >
                Brands
              </button>
            </h2>
            <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingTwo">
              <div className="accordion-body row gap-2 m-0 p-0 py-3" style={{ backgroundColor: "#f7f7f7" }}>
                {this.state.brands.map((brand) => (
                  <div className="d-flex gap-2 align-items-center">
                    <input type="checkbox" id="rating-5" className="col-1 h-75" value={brand} onChange={() => this.changeBrandsFilter(brand)}></input>
                    <label className="fs-12px">{brand}</label>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div class="accordion mt-2" id="filter-options">
          <div class="accordion-item">
            <h2 class="accordion-header" id="panelsStayOpen-headingThree">
              <button
                class="accordion-button py-2 px-2 text-dark filter-head"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#panelsStayOpen-collapseThree"
                aria-expanded="true"
                aria-controls="panelsStayOpen-collapseThree"
              >
                Price
              </button>
            </h2>
            <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingThree">
              <div className="accordion-body row gap-2 m-0 p-0 py-3" style={{ backgroundColor: "#f7f7f7" }}>
                {this.state.price >= 1 && (
                  <div className="d-flex gap-2 align-items-center">
                    <input type="checkbox" id="rating-5" value="50" className="col-1 h-75" onChange={() => this.changePriceFilter(50)}></input>
                    <label className="fs-12px">Rs 21 to Rs 50</label>
                  </div>
                )}
                {this.state.price >= 2 && (
                  <div className="d-flex gap-2 align-items-center">
                    <input type="checkbox" id="rating-5" value="100" className="col-1 h-75" onChange={() => this.changePriceFilter(100)}></input>
                    <label className="fs-12px">Rs 51 to Rs 100</label>
                  </div>
                )}

                {this.state.price >= 3 && (
                  <div className="d-flex gap-2 align-items-center">
                    <input type="checkbox" id="rating-5" value="200" className="col-1 h-75" onChange={() => this.changePriceFilter(200)}></input>
                    <label className="fs-12px">Rs 101 to Rs 200</label>
                  </div>
                )}
                {this.state.price >= 4 && (
                  <div className="d-flex gap-2 align-items-center">
                    <input type="checkbox" id="rating-5" value="500" className="col-1 h-75" onChange={() => this.changePriceFilter(500)}></input>
                    <label className="fs-12px">Rs 201 to Rs 500</label>
                  </div>
                )}
                {this.state.price >= 5 && (
                  <div className="d-flex gap-2 align-items-center">
                    <input type="checkbox" id="rating-5" value="501" className="col-1 h-75" onChange={() => this.changeBrandsFilter(501)}></input>
                    <label className="fs-12px">More than Rs 500</label>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        {this.state.discount && (
          <div class="accordion mt-2" id="filter-options">
            <div class="accordion-item">
              <h2 class="accordion-header" id="panelsStayOpen-headingFour">
                <button
                  class="accordion-button py-2 px-2 text-dark filter-head"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#panelsStayOpen-collapseFour"
                  aria-expanded="true"
                  aria-controls="panelsStayOpen-collapseFour"
                >
                  Discount
                </button>
              </h2>
              <div id="panelsStayOpen-collapseFour" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingFour">
                <div className="accordion-body row gap-2 m-0 p-0 py-3" style={{ backgroundColor: "#f7f7f7" }}>
                  <div className="d-flex gap-2 align-items-center">
                    <input type="checkbox" id="rating-5" className="col-1 h-75"></input>
                    <label className="d-flex fs-12px">Upto 5%</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </>
    );
  }
}

export default Filter;
