import Bblogo from "../images/bb_logo.png";
import { Link } from "react-router-dom";
import React from "react";

class Header extends React.Component {
  handleKeyEnter = (event) => {
    if (event.key === "Enter") {
      this.props.history.push(`/search/${event.target.value}`);
    }
  };
  render() {
    return (
      <>
        <nav className="navbar sticky-top w-100 bg-white px-2 shadow-sm d-sm-none d-md-block d-none">
          <div className="d-flex w-100 container justify-content-start align-items-stretch gap-md-2 gap-lg-4 ">
            <div className="">
              <Link to={"/"}>
                <img src={Bblogo} className="bb-logo display-block" alt="logo" />
              </Link>
            </div>
            <div className="border d-flex align-items-center rounded gap-2 flex-grow-1">
              <i className="search-svg ms-2"></i>
              <input
                type="search"
                className="border-0 outline-0 flex-grow-1"
                style={{ fontSize: "15px" }}
                placeholder="Search for products"
                onKeyDown={this.handleKeyEnter}
              />
            </div>
            <div className="location p-1 px-lg-4 rounded">
              <div>
                <span>
                  <i className="bike-svg"></i>
                </span>
                <span className="fw-bold">Get it in 3hrs</span>
              </div>
              <div>
                <span className="fw-bold">Home: </span>
                <span>560085, Bangalore</span>
              </div>
            </div>
            {!this.props.isLogged && (
              <div className="fs-10px bg-dark text-white w-10 px-4 text-center border d-flex align-items-center rounded">
                <button className="m-0 border-0 bg-dark text-white fw-bold" onClick={this.props.showAuth}>
                  Login / Sign up
                </button>
              </div>
            )}
            {this.props.isLogged && (
              <div
                className="position-relative fs-10px bg-dark text-white w-10 px-4 text-center border d-flex align-items-center rounded"
                id="dropdownMenuButton2"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                role="button"
              >
                <button className="m-0 border-0 bg-dark text-white fw-bold">Hi, {this.props.userName}</button>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                  <li>
                    <p className="dropdown-item" onClick={this.props.logout} role="button">
                      Log out
                    </p>
                  </li>
                </ul>
              </div>
            )}
            <Link to={"/cart"} className="d-flex align-items-center px-2 rounded cart">
              <div className="d-flex">
                <i className="cart-svg">
                  <span className="bg-dark text-white cart-number">{this.props.cartSize}</span>
                </i>
              </div>
            </Link>
          </div>
        </nav>
        <nav className="p-2 shadow-sm d-md-none d-lg-none mobile-nav">
          <div className="d-flex justify-content-between">
            <div></div>
            <Link to={"/"}>
              <span className="text-white">bigbasket</span>
            </Link>

            {this.props.isLogged && (
              <i className="mobile-svg user-svg" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight"></i>
            )}
            {!this.props.isLogged && <i className="mobile-svg user-svg" role="button" onClick={this.props.showAuth}></i>}
          </div>
          <div className="border d-flex align-items-center rounded gap-2 flex-grow-1 mt-2 bg-white">
            <i className="search-svg bg-white ms-2"></i>
            <input
              type="search"
              className="border-0 outline-0 flex-grow-1"
              style={{ fontSize: "15px" }}
              placeholder="Search for products"
              onKeyDown={this.handleKeyEnter}
            />
          </div>
          <div className="offcanvas offcanvas-end w-50" tabIndex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
            <div className="offcanvas-body">
              <p className="border-bottom">Hi, {this.props.userName}</p>
              <p onClick={this.props.logout} role="button" data-bs-toggle="offcanvas">
                Log Out
              </p>
              <Link to={"/cart"}>
                <p data-bs-toggle="offcanvas">Your Cart</p>
              </Link>
            </div>
          </div>
        </nav>
      </>
    );
  }
}

export default Header;
