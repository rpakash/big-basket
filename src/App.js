import Footer from "./Components/Footer";
import Header from "./Components/Header";
import Home from "./Components/Home";
import { Route } from "react-router-dom";
import Search from "./Components/Search";
import Cart from "./Components/Cart";
import React from "react";
import Auth from "./Components/Auth";
import Products from "./data/produts.json";
import SingleProduct from "./Components/SingleProduct";
import toast, { Toaster } from "react-hot-toast";
import Category from "./Components/Category";
import Checkout from "./Components/Checkout";
import { withRouter } from "react-router-dom";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      isLogged: false,
      showAuth: false,
      notifyMessage: "",
      products: Products,
      cart: new Map(),
    };

    if (localStorage.getItem("currentUser")) {
      this.state.user = JSON.parse(localStorage.getItem("currentUser"));
      this.state.isLogged = true;
    }
  }

  clearCart = () => {
    this.setState({
      cart: new Map(),
    });
  };

  setUser = (user) => {
    toast.success("User logged in successfully");
    let redirect = this.state.redirect;

    if (redirect) {
      this.props.history.push(redirect);
    }

    this.setState({
      user: user,
      isLogged: true,
      showAuth: false,
      redirect: undefined,
    });
  };

  logOut = () => {
    toast.success("User logged out successfully");

    this.setState({
      user: {},
      isLogged: false,
    });
    localStorage.removeItem("currentUser");
  };

  showAuth = (redirect) => {
    this.setState({
      showAuth: true,
      redirect: redirect,
    });
  };

  closeAuth = () => {
    console.log("closing");
    this.setState({
      showAuth: false,
    });
  };

  addProduct = (productKey, product) => {
    toast.success("An item has been added to your basket successfully", {
      style: {
        background: "#e4f1cc",
        color: "#76b900",
      },
      position: "bottom-center",
    });
    this.setState((state) => {
      if (state.cart.has(productKey)) {
        let product = state.cart.get(productKey);
        return {
          cart: state.cart.set(productKey, { ...product, quantity: +product.quantity + 1 }),
        };
      } else {
        return {
          cart: state.cart.set(productKey, { ...product, quantity: 1 }),
        };
      }
    });
  };

  decreaseProductQuantity = (productKey) => {
    toast.success("Quantity of this item has been reduced", {
      style: {
        background: "#e4f1cc",
        color: "#76b900",
      },
      position: "bottom-center",
    });
    if (this.state.cart.has(productKey)) {
      this.setState((state) => {
        let product = state.cart.get(productKey);
        if (product.quantity > 1) {
          return {
            cart: state.cart.set(productKey, { ...product, quantity: +product.quantity - 1 }),
          };
        } else {
          state.cart.delete(productKey);
          return {
            cart: state.cart,
          };
        }
      });
    }
  };

  removeProductFromCart = (productKey) => {
    if (this.state.cart.has(productKey)) {
      this.setState((state) => {
        state.cart.delete(productKey);
        return {
          cart: state.cart,
        };
      });
    }
  };

  render() {
    return (
      <div className="App">
        <Route
          render={(props) => (
            <Header
              {...props}
              showAuth={this.showAuth}
              isLogged={this.state.isLogged}
              userName={this.state.user.firstName}
              logout={this.logOut}
              cartSize={this.state.cart.size}
            ></Header>
          )}
        />
        {this.state.showAuth && <Auth setUser={this.setUser} close={this.closeAuth}></Auth>}
        <Route
          path={"/product/:name"}
          render={(props) => (
            <SingleProduct
              {...props}
              products={this.state.products}
              cart={this.state.cart}
              addProduct={this.addProduct}
              decreaseProductQuantity={this.decreaseProductQuantity}
              removeProductFromCart={this.removeProductFromCart}
            />
          )}
        />
        <Route
          path={"/"}
          exact
          render={() => (
            <Home
              products={this.state.products}
              cart={this.state.cart}
              addProduct={this.addProduct}
              decreaseProductQuantity={this.decreaseProductQuantity}
            />
          )}
        />
        <Route
          path={"/search/:query"}
          render={(props) => (
            <Search
              {...props}
              products={this.state.products}
              cart={this.state.cart}
              addProduct={this.addProduct}
              decreaseProductQuantity={this.decreaseProductQuantity}
            />
          )}
        />
        <Route
          path={"/cart"}
          render={(props) => (
            <Cart
              {...props}
              cart={this.state.cart}
              products={this.state.products}
              addProduct={this.addProduct}
              decreaseProductQuantity={this.decreaseProductQuantity}
              removeProduct={this.removeProductFromCart}
              isLogged={this.state.isLogged}
              showAuth={this.showAuth}
            />
          )}
        />
        <Route
          path={"/category/:category"}
          render={(props) => (
            <Category
              {...props}
              products={this.state.products}
              cart={this.state.cart}
              addProduct={this.addProduct}
              decreaseProductQuantity={this.decreaseProductQuantity}
            />
          )}
        />
        <Route
          path={"/checkout"}
          render={(props) => <Checkout {...props} cart={this.state.cart} isLogged={this.state.isLogged} clearCart={this.clearCart} />}
        />
        <Toaster></Toaster>
        <Route path={"/"} render={(props) => <Footer {...props}></Footer>} />
      </div>
    );
  }
}

export default withRouter(App);
